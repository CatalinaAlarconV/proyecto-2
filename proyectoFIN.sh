#!/bin/bash

echo "______________________________________________________________________________"
echo "..........................Visualizador de proteinas..........................."
echo "______________________________________________________________________________"

#se pide un código al usuario se pasa a mayúscula y se guardar en "$codigo"
echo "             ingrese el código de la proteina a visualizar          "
read codigo
codigo=${codigo^^}

#se revisa si existe en la base de datos, si no existe, informa del error y sale del programa.
if [[ ! -f bd-pdb.txt ]]; then
	echo "error: la base de datos no existe"
	exit 1
fi

#se extrae la información de la base de datos donde esté "$codigo" y se almacena en "$informacion", muestra la información al usuario.
informacion=$(grep -w $codigo bd-pdb.txt)
echo $informacion
#se revisa si $informacion contiene el texto "Protein",si el código corresponde a una proteína. si no es así, informa del error y saler.
if ! [[ $informacion =~ '"Protein"' ]]; then
	echo "EL codigo ingresado no pertenece a una proteina"
	exit 1
fi

#revisa si es que no se ha descargado la proteína. si es que no existe,se descarga. de lo contrario, informa que se saltará la descarga.
if [[ ! -f $codigo.pdb ]]; then
	echo "Se descargara $codigo"
	wget https://files.rcsb.org/download/$codigo.pdb
else
	echo "la proteina ya existe, saltando descarga"
fi


#se sacan todas las líneas que comiencen con ATOM del .pdb y se guardan en un .txt
#echo "se va a procesar la proteina $codigo"
#grep ^ATOM $codigo.pdb > $codigo.txt
#llama a awk para que corra filtrar.awk en el .txt que contiene todas las líneas que comienzan con ATOM y redirecciono a un archivo dot.
#awk -f filtrar.awk $codigo.txt 

#Se busca los textos HETATM y no se toma los HOH
echo ""
awk '($1 == "HETATM") && ($4 != "HOH") {print $0}' $codigo.pdb > hethoh.pdb

awk 'BEGIN {CONT=0; lig=""; sumCoor=0}
	{ 
		{
			sumCoor=sumCoor+$7+$8+$9; 
			CONT++;
		} 
		if (lig != $4) {
			lig=$4; 
			print (lig, "CG:" sumCoor/CONT); 
			CONT=0; 
			sumCoor=0;
		} 
		else {
			lig=$4;
			CONT++; 
			sumCoor=sumCoor+$7+$8+$9; 
		}
	}' hethoh.pdb


#cat x.txt
#cat y.txt
#cat z.txt

#awk ' $3 = / [O]+ / ' hethoh.pdb > ionzn.txt
#cat hethoh.pdb
