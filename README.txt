								VISUALIZADOR DE PROTEINAS


Este es un programa creado para  visualizar gráficos de la distancia que tiene los aminoacidos que componen las proteinas generados a partir de una base de datos obtenidos de pdb. Como también está hecho para detectar presencia de iones y ligandos.



1- Ejecute programa proyecto2final.sh en la consola.

2- Al ejecutar el programa el usuario deberá escribir el codigo de la proteina que desee visualizar.

3- EL programa arrojará los resultados de que iones que lo componen como también ligandos en la estructura.

4- El usuario deberá ingresar la distancia de los elementos.

4- Se iniciaran cálculos de centro geometrico para ligandos sumando vectores X, Y, Z dividido por el numero total de filas.


